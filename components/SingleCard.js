import React from 'react';
import { ScrollView, StyleSheet, Image, View, Text, TouchableHighlight } from 'react-native';

export default class SingleCard extends React.Component {

    render() {
        return (
            <View>
                <View style={styles.snippitc}>
                    <Text style={{ fontSize: 12, fontWeight: 'bold', paddingTop: 10, paddingLeft: 4, color: '#535252' }}>{this.props.data.title}</Text>
                    <Text style={{ fontSize: 12, padding: 5, color: '#535252' }}>{this.props.data.body}</Text>
                    <Text />
                    {/* <Text style={{ fontSize: 12, fontWeight: 'bold', paddingTop: 10, paddingLeft: 4, color: '#535252' }}>Active Milestone</Text>
                    <Text style={{ fontSize: 12, padding: 5, color: '#535252' }}>You must have a PenPal before you can create your next Milestone.</Text>
                    <Text />
                    <View style={{ borderBottomColor: '#515151', borderBottomWidth: .5 }} />
                    <Text style={{ fontSize: 10, padding: 5, color: '#535252' }}> Creator: {`${this.props.data.creator.First_Name} ${this.props.data.creator.Last_Name}`}</Text>
                    <Text style={{ fontSize: 10, padding: 5, paddingTop: 1, color: '#535252' }}> PenPal: No PenPal matched yet. Check back later.</Text> */}
                </View>
            </View>
        )
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 15,
        backgroundColor: '#fff',
    },
    container1: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    snippitc: {
        borderWidth: 1,
        borderRadius: 3,
        minHeight: 50,
        margin: 15,
        marginBottom: 4,
        borderColor: '#515151'
    },
    avatar: {
        height: 100,
        borderRadius: 500,
        borderWidth: 60,
        width: 100,
    },
    imageContainer: {
        flex: 1,
        alignItems: 'flex-end'
    },
    refresh: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end'
    }
});






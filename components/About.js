import React from 'react';

import { ScrollView, StyleSheet, Image, View, Text, TouchableHighlight } from 'react-native';



export default class About extends React.Component {

    render() {
        return (
            <View>
                <TouchableHighlight onPress={() => this.props.func()}>
                    <Text style={{ color: '#2A3DA8', fontSize: 10, marginLeft: 10, marginTop: 20 }}>back to Launch a Project</Text>
                </TouchableHighlight>
                <Text style={{ fontSize: 37, marginLeft: 10, marginTop: 19 }}>How it Works</Text>
                <Text style={{ fontSize: 19, marginTop: 10, marginLeft: 10 }}>1. Launch Your Project</Text>
                <Text style={{ marginLeft: 10, marginRight: 10, marginTop: 4 }}>Launch your Project. Provide a title and short description of the Project.</Text>
                <View style={styles.container1}>
                    <Image source={require('../assets/images/slingshot.png')} style={{ marginTop: 20, width: 60, height: 60 }} />
                </View>
                <Text style={{ fontSize: 19, marginTop: 24, marginLeft: 10 }}>2. Get Matched to a PenPal.</Text>
                <Text style={{ marginLeft: 10, marginRight: 10, marginTop: 4 }}>Collaborizm PenPal's matching algorithm will pair you to an optimal match for each Quick-Discussion on your Project.</Text>
                <View style={styles.container1}>
                    <Image source={require('../assets/images/worldwide.png')} style={{ marginTop: 20, width: 60, height: 60 }} />
                </View>
                <Text style={{ fontSize: 19, marginTop: 24, marginLeft: 10 }}>3. Help Each Other Make Progress</Text>
                <Text style={{ marginLeft: 10, marginRight: 10, marginTop: 4 }}>• Select a Quick Discussion topic for your Project.</Text>
                <Text style={{ paddingTop: 8, marginLeft: 10, marginRight: 10, marginTop: 4 }}>• Use the PenPal chat to provide advice and encouragement to your PenPals.</Text>
                <Text style={{ paddingTop: 8, marginLeft: 10, marginRight: 10, marginTop: 4 }}>• ONCE both of you feel your Quick Discussion is complete, you will move on to a new Quick Discussion with a different PenPal. How cool is that?</Text>
                <View style={styles.container1}>
                    <Image source={require('../assets/images/profit.png')} style={{ marginTop: 20, width: 60, height: 60 }} />
                </View>
            </View>
        )
    };
}

const styles = StyleSheet.create({
    container1: {
        //flex: 1,
        flexDirection: 'row',
        justifyContent: 'center'
    },
})
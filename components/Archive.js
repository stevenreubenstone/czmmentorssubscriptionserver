import React from 'react';
import { Mutation } from "react-apollo";
import { ARCHIVE_SNIPPIT } from "../queries.js";
import { ScrollView, StyleSheet, Image, View, Text, TouchableHighlight } from 'react-native';



export default class Archive extends React.Component {

  render() {
    return (
      <Mutation mutation={ARCHIVE_SNIPPIT}>
        {archiveSnippit => (
          <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text style={{ fontSize: 8, marginLeft: 15 }}></Text>
            <TouchableHighlight onPress={() => archiveSnippit({ variables: { id: this.props.snippit } })}>
              <Text style={{ fontSize: 8, marginRight: 15 }} >archive project</Text>
            </TouchableHighlight>
          </View>
        )}
      </Mutation>
    )
  };
}


import React from 'react';
import milestones from '../milestones';
import { ScrollView, StyleSheet, Image, View, Text, TouchableHighlight } from 'react-native';


export default class Tip extends React.Component {

    render() {

        return (
            <View>
                <TouchableHighlight onPress={() => this.props.close(false)}>
                    <Text style={{ color: '#2A3DA8', fontSize: 10, marginLeft: 10, marginTop: 20 }}>back</Text>
                </TouchableHighlight>
                <Text style={{ fontSize: 27, marginLeft: 10, marginTop: 19 }}>Milestones: The PenPal Way.</Text>
                <Text style={{ marginLeft: 10, marginRight: 10, marginTop: 4 }}>Milestones should take between a few hours and a few days to complete. (click to select)</Text>
                <Text />
                <Text />

                {milestones.map(item => {
                    return <TouchableHighlight onPress={() => {
                        this.props.add(item);
                        this.props.close(false);
                    }}>
                        <Text style={{ padding: 9 }}>{item}</Text>
                    </TouchableHighlight>
                })}
            </View>
        )
    };
}

const styles = StyleSheet.create({
    container1: {
        //flex: 1,
        flexDirection: 'row',
        justifyContent: 'center'
    },
})
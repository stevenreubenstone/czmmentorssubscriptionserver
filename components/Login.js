import React, { Component } from 'react';
import { Constants, WebBrowser } from 'expo';
import {
    Image,
    Linking,
    StyleSheet,
    Platform,
    Text,
    View,
    TouchableHighlight
} from 'react-native';
import { Button } from 'react-native-elements';

export default class Login extends Component {

    state = {
        user: undefined
    }

    async fbLogIn() {
        try {
            const {
                type,
                token,
                expires,
                permissions,
                declinedPermissions,
            } = await Expo.Facebook.logInWithReadPermissionsAsync('301863613354615', {
                permissions: ['public_profile'],
            });
            if (type === 'success') {
                const result = await Expo.SecureStore.setItemAsync("fbToken", token);
                this.props.refetch();

            } else {
                // type === 'cancel'
            }
        } catch ({ message }) {
            alert(`Facebook Login Error: ${message}`);
        }
    }


    render() {
        return (
            <View style={styles.container}>
                <View style={styles.content}>
                    <Image source={require('../assets/images/Rex.png')} style={{ width: 125, height: 125 }} />

                    <Text style={styles.header}>
                        Welcome to Collaborizm PenPals.
              </Text>
                    <Text style={styles.text}>
                        Please log in to continue {'\n'}
                        to the awesomness.
              </Text>
                </View>
                <View style={styles.buttons}>
                    <Button
                        onPress={() => {
                            this.fbLogIn()
                            // this._handlePressButtonAsync()
                        }}
                        title="Login with FB"
                        buttonStyle={{
                            backgroundColor: "rgba(255, 255, 255, 1)",
                            width: 150,
                            height: 50,
                            borderColor: "#676767",
                            borderWidth: 0.5,
                            borderRadius: 5,
                        }}
                        color="#841584"
                        accessibilityLabel="Learn more about this purple button"
                    />
                </View>
            </View>
        );
    }
}

const iconStyles = {
    borderRadius: 10,
    iconStyle: { paddingVertical: 5 },
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
    },
    content: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    avatar: {
        margin: 20,
    },
    avatarImage: {
        borderRadius: 50,
        height: 100,
        width: 100,
    },
    header: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    text: {
        textAlign: 'center',
        color: '#333',
        marginBottom: 5,
    },
    buttons: {
        justifyContent: 'center',
        flexDirection: 'row',
        margin: 20,
        marginBottom: 30,
    },
});
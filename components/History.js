import React from 'react';
import { Query } from "react-apollo";
import { HISTORY } from "../queries";
import HistoryCard from './HistoryCard';
import {
    Image,
    Platform,
    ScrollView,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    TouchableHighlight,
    View,
} from 'react-native';


export default class History extends React.Component {

    clearSecureStore = async () => {
        const result = await Expo.SecureStore.deleteItemAsync("fbToken")
        return null
    }

    render() {
        return (
            <View>
                <Text style={{ marginLeft: 10, marginTop: 9, fontSize: 15 }}>My Project History</Text>
                <Text />
                {this.props.data.getUserSnippits.map(item => {
                    if (item.milestones.length === 0) { return null }
                    return <HistoryCard data={item} />
                }
                )}
                <Text style={{ marginLeft: 10, marginTop: 9, fontSize: 9 }}>Click History on top right to go back to Milestones tab.</Text>
                <TouchableHighlight onPress={() => this.clearSecureStore()}>
                    <Text style={{ marginLeft: 10, marginTop: 50 }}>Clear Secure Store (dev mode)</Text>
                </TouchableHighlight>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 15,
        backgroundColor: '#fff',
    },
    container1: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    snippitc: {
        borderWidth: 1,
        borderRadius: 3,
        minHeight: 100,
        margin: 15,
        marginBottom: 4,
        borderColor: '#515151',
        backgroundColor: '#fff'
    },
    avatar: {
        height: 100,
        borderRadius: 500,
        borderWidth: 60,
        width: 100,
    },
    imageContainer: {
        flex: 1,
        alignItems: 'flex-end'
    },
    refresh: {
        // flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
});
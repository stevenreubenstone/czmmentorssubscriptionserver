import React from 'react';
import {
    Modal,
    Image,
    Picker,
    Platform,
    ScrollView,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    TouchableHighlight,
    View,
} from 'react-native';
import { Mutation } from "react-apollo";
import { SKIP } from "../queries.js";


export default class SkipModal extends React.Component {


    render() {
        return (
            <Mutation mutation={SKIP}>
                {skip => (
                    <View>
                        <TouchableOpacity onPress={() => this.props.func(false)}>
                            <Text style={{margin: 14, fontSize: 16 }}>Cancel</ Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => {
                            skip({ variables: { snippit_id: this.props.project } });
                            this.props.func(false);
                        }}>
                            <Text style={{ margin: 14, fontSize: 16 }}>Yes, I would like to skip.</ Text>
                        </TouchableOpacity>
                    </View>
                )}
            </Mutation>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 15,
        backgroundColor: '#fff',
    },
    flexPicker: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-start'
    },
    container1: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    snippitc: {
        borderWidth: 1,
        borderRadius: 3,
        minHeight: 100,
        margin: 15,
        marginBottom: 4,
        borderColor: '#515151',
        backgroundColor: '#fff'
    },
    avatar: {
        height: 100,
        borderRadius: 500,
        borderWidth: 60,
        width: 100,
    },
    imageContainer: {
        flex: 1,
        alignItems: 'flex-end'
    },
    outline: {
        borderWidth: 1,
        borderRadius: 3,
        minHeight: 100,
        margin: 8,
        marginBottom: 4,
        borderColor: '#515151',
        backgroundColor: '#fff'
    },
    outline2: {
        borderWidth: 1,
        borderRadius: 3,
        minHeight: 100,
        margin: 8,
        marginBottom: 4,
        borderColor: '#515151',
        backgroundColor: '#323234'
    },
});

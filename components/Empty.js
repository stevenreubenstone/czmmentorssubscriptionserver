import React from 'react';

import { ScrollView, StyleSheet, Image, View, Text, TouchableHighlight } from 'react-native';


export default class Empty extends React.Component {

    render() {
        return (
            <View style={styles.snippitc}>
                <Text style={{ padding: 10 }}>You must have atleast one Active PenPal match to see your Milestone cards.</Text>
            </View>
        )
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 15,
        backgroundColor: '#fff',
    },
    flexPicker: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-start'
    },
    container1: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    snippitc: {
        borderWidth: 1,
        borderRadius: 3,
        minHeight: 40,
        margin: 15,
        marginBottom: 4,
        borderColor: '#515151',
        backgroundColor: '#fff',
        borderStyle: 'dotted'
    },
    avatar: {
        height: 100,
        borderRadius: 500,
        borderWidth: 60,
        width: 100,
    },
    imageContainer: {
        flex: 1,
        alignItems: 'flex-end'
    },
    outline: {
        borderWidth: 1,
        borderRadius: 3,
        minHeight: 100,
        margin: 8,
        marginBottom: 4,
        borderColor: '#515151',
        backgroundColor: '#fff'
    },
    outline2: {
        borderWidth: 1,
        borderRadius: 3,
        minHeight: 100,
        margin: 8,
        marginBottom: 4,
        borderColor: '#515151',
        backgroundColor: '#323234'
    },
});

import React from 'react';
import {
    Image,
    Picker,
    Platform,
    ScrollView,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    TouchableHighlight,
    View,
} from 'react-native';
import { CheckBox } from 'react-native-elements'
import { Mutation } from "react-apollo";
import { CHECK_MILESTONE } from "../queries.js";


export default class Checker extends React.Component {

    render() {
        return (
            <Mutation mutation={CHECK_MILESTONE}>
                {checkMilestone => (
                    <View>
                        <Text style={{ margin: 14, fontSize: 20 }}>Are you positive you want to mark this milestone as complete (this can not be undone)?</Text>
                        <TouchableHighlight
                            onPress={() => {
                                checkMilestone({ variables: { milestone_id: this.props.palmilestone, mymilestone: this.props.mymilestone } });
                                this.props.check();
                                this.props.func(false);
                            }}>
                            <Text style={{ margin: 14, fontSize: 16 }}>Yes, I have discussed with my PenPal, and we agree it is complete!</Text>
                        </TouchableHighlight>
                        <Text />
                        <Text />
                        <TouchableHighlight
                            onPress={() => {
                                this.props.func(false);
                            }}>
                            <Text style={{ margin: 14, fontSize: 16 }}>Cancel</Text>
                        </TouchableHighlight>
                    </View>
                )}
            </Mutation>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 15,
        backgroundColor: '#fff',
    },
    flexPicker: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-start'
    },
    container1: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    snippitc: {
        borderWidth: 1,
        borderRadius: 3,
        minHeight: 100,
        margin: 15,
        marginBottom: 4,
        borderColor: '#515151',
        backgroundColor: '#fff'
    },
    avatar: {
        height: 100,
        borderRadius: 500,
        borderWidth: 60,
        width: 100,
    },
    imageContainer: {
        flex: 1,
        alignItems: 'flex-end'
    },
    outline: {
        borderWidth: 1,
        borderRadius: 3,
        minHeight: 100,
        margin: 8,
        marginBottom: 4,
        borderColor: '#515151',
        backgroundColor: '#fff'
    }
});

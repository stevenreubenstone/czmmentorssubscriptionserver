import React from 'react';
import {
    Modal,
    Image,
    Picker,
    Platform,
    ScrollView,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    TouchableHighlight,
    View,
} from 'react-native';
import { CheckBox } from 'react-native-elements'
import { Mutation } from "react-apollo";
import { UPDATE_MILESTONE } from "../queries.js";
import Checker from "./Checker";
import Tip from '../components/Tip';
import Skip from '../components/Skip';

export default class ProgressCard extends React.Component {

    state = {
        checked: this.props.datam.pair.milestone.completed,
        milestone: this.props.datam.milestone.body,
        modalVisible: false,
        modal2Visible: false,
    }

    add = (data) => {
        this.setState({ milestone: data })
    }

    check = () => {
        this.setState({ checked: true })
    }

    setModalVisible = (visible) => {
        this.setState({ modalVisible: visible });
    }

    setModal2Visible = (visible) => {
        this.setState({ modal2Visible: visible });
    }

    render() {
        const milestone = this.state.milestone
        const milestone_id = this.props.datam.milestone.id


        return (
            <Mutation mutation={UPDATE_MILESTONE}>
                {updateMilestone => (
                    <View>
                        <Modal
                            animationType="slide"
                            transparent={false}
                            visible={this.state.modalVisible}
                            onRequestClose={() => {
                                Alert.alert('Modal has been closed.');
                            }}>
                            <View style={{ marginTop: 22 }}>
                                <Checker func={this.setModalVisible} palmilestone={this.props.datam.pair.milestone.id} check={this.check} mymilestone={milestone_id} />
                            </View>
                        </Modal>

                        <Modal
                            animationType="slide"
                            transparent={false}
                            visible={this.state.modal2Visible}
                            onRequestClose={() => {
                                Alert.alert('Modal has been closed.');
                            }}>
                            <View style={{ marginTop: 22 }}>
                                <Tip add={this.add} close={this.setModal2Visible} />
                            </View>
                        </Modal>
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <Image
                                    style={{ width: 32, height: 32, borderRadius: 16, marginLeft: 7 }}
                                    source={{ uri: `${this.props.datam.creator.user_avatar}` }}
                                />
                                <Image
                                    style={{ width: 32, height: 32, borderRadius: 16, marginLeft: 7 }}
                                    source={{ uri: `${this.props.datam.pair.creator.user_avatar}` }}
                                />
                            </View>
                        </View>
                        <View style={styles.outline}>
                            <Text style={{ padding: 4, marginLeft: 4, fontSize: 15, marginTop: 5 }}>My Project: {this.props.datam.title}</Text>
                            <Text style={{ padding: 4, marginLeft: 4, fontSize: 15 }}>PenPal: {this.props.datam.pair.creator.First_Name} {this.props.datam.pair.creator.Last_Name}</Text>
                            <Text style={{ padding: 4, marginLeft: 4, fontSize: 15 }}>PenPal's Project: {this.props.datam.pair.title}</Text>



                            <Text style={{ fontWeight: 'bold', margin: 8, marginBottom: 4, marginTop: 14 }}>{`${this.props.datam.pair.creator.First_Name}'s Selected Mini-Milestone`}</Text>
                            <Text style={{ margin: 8, marginBottom: 4 }}>{(!this.props.datam.pair.milestone.body) ? 'Encourage your PenPal to create a new mini-milestone in the PenPal chat!' : `${this.props.datam.pair.milestone.body}`}</Text>
                            {
                                (this.state.checked == false) ?
                                    <CheckBox
                                        title={`Click Here if ${this.props.datam.pair.creator.First_Name} completed their Milestone.`}
                                        checked={this.state.checked}
                                        containerStyle={{ backgroundColor: '#fff', borderWidth: .5, marginTop: 5 }}
                                        onPress={() => this.setModalVisible(true)}
                                        textStyle={{ fontSize: 10, fontWeight: 'normal' }}
                                    />
                                    :
                                    <CheckBox
                                        title={`You have marked this milestone as complete. (This can not be changed)`}
                                        checked={this.state.checked}
                                        containerStyle={{ backgroundColor: '#fff', borderWidth: 0.5, marginTop: 5 }}
                                        textStyle={{ fontSize: 10, fontWeight: 'normal' }}
                                    />
                            }
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', marginTop: 14 }}>
                                <Text style={{ fontWeight: 'bold', margin: 8, marginBottom: 0, marginRight: 0 }}>My Mini-Milestone</Text>
                                <View>
                                    <TouchableHighlight onPress={() => this.setModal2Visible(true)}>
                                        <View style={{ margin: 8, marginBottom: 0, flex: 1, flexDirection: 'row', justifyContent: 'center', height: 20, fontSize: 8, backgroundColor: '#6A7A94', borderRadius: 3 }}>
                                            <Text style={{ paddingTop: 3, paddingLeft: 4, paddingRight: 4, color: '#fff', fontSize: 10 }}>+ from Milestone Library</Text>
                                        </View>
                                    </TouchableHighlight>
                                </View>
                            </View>
                            <TextInput
                                style={{ borderColor: 'gray', borderWidth: 1, borderRadius: 4, margin: 8, height: 90, padding: 7 }}
                                multiline={true} placeholder="Write your mini-milestone here, OR select one from the list above!" value={milestone} onChangeText={(text) => this.setState({ milestone: text })}
                            />
                            <TouchableHighlight onPress={() => { updateMilestone({ variables: { milestone: milestone, milestone_id: milestone_id } }) }}>
                                <View style={{ marginBottom: 9, flex: 1, flexDirection: 'row', justifyContent: 'center', height: 20, fontSize: 9, backgroundColor: '#39ACF4', marginLeft: 120, marginRight: 120, borderRadius: 3 }}>
                                    <Text style={{ paddingTop: 3, color: '#fff', fontSize: 10 }}>Save</Text>
                                </View>
                            </TouchableHighlight>
                        </View>
                        <Skip project={this.props.datam.id} />
                        <View style={styles.container1}>
                            <Text style={{ fontSize: 25, marginBottom: 25, }}>...</Text>
                        </View>
                    </View>
                )}
            </Mutation>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 15,
        backgroundColor: '#fff',
    },
    flexPicker: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-start'
    },
    container1: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    snippitc: {
        borderWidth: 1,
        borderRadius: 3,
        minHeight: 100,
        margin: 15,
        marginBottom: 4,
        //borderColor: '#515151',
        backgroundColor: '#fff'
    },
    avatar: {
        height: 100,
        borderRadius: 500,
        borderWidth: 60,
        width: 100,
    },
    imageContainer: {
        flex: 1,
        alignItems: 'flex-end'
    },
    outline: {
        // borderWidth: 1,
        shadowColor: '#515151',
        borderRadius: 3,
        minHeight: 100,
        margin: 8,
        marginBottom: 4,
        // borderColor: '#515151',
        backgroundColor: '#fff'
    },
    outline2: {
        borderWidth: 1,
        borderRadius: 3,
        minHeight: 100,
        margin: 8,
        marginBottom: 4,
        borderColor: '#515151',
        backgroundColor: '#323234'
    },
});

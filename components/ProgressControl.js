import React from 'react';
import ProgressCard from './ProgressCard';
import Empty from './Empty';
import {
    Image,
    Platform,
    ScrollView,
    TouchableHighlight,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View,
} from 'react-native';


export default class ProgressControl extends React.Component {

    render() {
        // create array of milestoned entries if lenght === 0 return empty state
        const array = this.props.data.getUserSnippits.filter(item => {
            return item.milestone
        })

        return (
            (array.length === 0) ? <Empty /> :
                <View>
                    {this.props.data.getUserSnippits.map(item => {
                        if (!item.milestone) {
                            return null
                        }
                        else {
                            return <ProgressCard datam={item} tip={this.props.tip} />
                        }
                    }
                    )}
                </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 15,
        backgroundColor: '#fff',
    },
    refresh: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    container1: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    snippitc: {
        borderWidth: 1,
        borderRadius: 3,
        minHeight: 100,
        margin: 15,
        marginBottom: 4,
        borderColor: '#515151',
        backgroundColor: '#fff'
    },
    avatar: {
        height: 100,
        borderRadius: 500,
        borderWidth: 60,
        width: 100,
    },
    imageContainer: {
        flex: 1,
        alignItems: 'flex-end'
    }
});
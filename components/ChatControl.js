import React from 'react';
import { Query } from "react-apollo";
import { ScrollView, StyleSheet, Image, View, Text, TouchableHighlight, TextInput, } from 'react-native';
import { GET_CHAT } from "../queries.js";
import { SUBSCRIBE_CHAT } from "../queries.js";
import Chat from "./Chat";

export default class ChatControl extends React.Component {
    static navigationOptions = {
        title: 'Chat',
    };

    render() {
        return (
            <Query query={GET_CHAT} variables={{ connection: this.props.connection }} fetchPolicy="network-only">
                {({ loading, error, data, subscribeToMore }) => {
                    if (loading) return <Text>Loading...</Text>;
                    if (error) return <Text>There is error</Text>;
                    return (
                        <Chat data={data.getChat} reset={this.props.reset} connection={this.props.connection}
                            subscribeToNewComments={() =>
                                subscribeToMore({
                                    document: SUBSCRIBE_CHAT,
                                    variables: { connection: this.props.connection },
                                    updateQuery: (prev, { subscriptionData }) => {
                                        if (!subscriptionData.data) return prev;
                                        const chatItem = subscriptionData.data.chatSent;
                                        return Object.assign({}, prev, {
                                            getChat: [...prev.getChat, chatItem]
                                        });
                                    }
                                })
                            } />
                    )
                }}
            </Query>
        )
    }
}


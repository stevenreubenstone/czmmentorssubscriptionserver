import React from 'react';
import { Modal, SafeAreaView, ScrollView, StyleSheet, Image, View, Text, TouchableHighlight, TextInput, KeyboardAvoidingView } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import AddChat from "./AddChat";
import ChatPost from "./ChatPost";

export default class ChatScreen extends React.Component {
    static navigationOptions = {
        title: 'Chat',
    };

    state = {
        modalVisible: false,
        chat_id: null,
    };

    setModalVisible = (visible, id) => {
        this.setState({ modalVisible: visible, chat_id: id });
    }

    componentDidMount() {
        this.props.subscribeToNewComments();
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding" enabled keyboardVerticalOffset={64}>
                    <ScrollView ref={ref => this.scrollView = ref}
                        style={{ flex: 1, marginTop: 8 }} onContentSizeChange={(contentWidth, contentHeight) => {
                            this.scrollView.scrollToEnd({ animated: true });
                        }}
                    >
                        <Modal
                            animationType="slide"
                            transparent={false}
                            visible={this.state.modalVisible}
                            onRequestClose={() => {
                                Alert.alert('Modal has been closed.');
                            }}>
                            <View style={{ marginTop: 22 }}>
                                <ChatPost chat={this.state.chat_id} func={this.setModalVisible} />
                                <View>
                                    <TouchableHighlight
                                        onPress={() => {
                                            this.setModalVisible(!this.state.modalVisible);
                                        }}>
                                        <Text style={{ marginLeft: 10, fontSize: 16 }}>Cancel</Text>
                                    </TouchableHighlight>
                                </View>
                            </View>
                        </Modal>
                        {this.props.data.map(item => {
                            return <View style={{ marginRight: 5 }} >
                                <TouchableHighlight onPress={() => {
                                    this.setModalVisible(true, item.id)
                                }}>
                                    <View style={styles.container1}>
                                        <View>
                                            <Image
                                                style={{ width: 30, height: 30, borderRadius: 15, marginLeft: 7 }}
                                                source={{ uri: `${item.user.user_avatar}` }}
                                            />
                                        </View>
                                        <View style={{ paddingLeft: 5, marginRight: 9 }}>
                                            <Text style={{ fontSize: 9 }}>{`${item.user.First_Name} ${item.user.Last_Name}`}</Text>

                                            <Text style={{ fontSize: 12, marginRight: 23 }}>{item.body}</Text>

                                            <Text style={{ marginTop: 6 }} />
                                        </View>
                                    </View>
                                </TouchableHighlight>
                            </View>
                        })}
                        <Text />
                        <Text />
                        <Text />
                    </ScrollView>
                    <TouchableHighlight onPress={() => { this.props.reset(22) }}>
                        <Text style={{ fontSize: 10 }}>back to PenPals</Text>
                    </TouchableHighlight>
                    <View style={{ justifySelf: 'flex-end' }}>
                        <AddChat connection={this.props.connection} />
                    </View>
                </KeyboardAvoidingView>
            </SafeAreaView>
        )

    }
}

const styles = StyleSheet.create({
    container1: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        marginRight: 10
    },
    container: {
        flex: 1
    },
})



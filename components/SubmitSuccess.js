import React from 'react';
import {
    Image,
    Platform,
    ScrollView,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    TouchableHighlight,
    View,
} from 'react-native';
import { Button } from 'react-native-elements';

export default class SnippitSuccess extends React.Component {
    render() {
        return (
            <View style={this.props.styler}>
                <View style={styles.container1}>
                    <Image source={require('../assets/images/project.png')} style={{ marginTop: 23, width: 65, height: 65 }} />
                </View>
                <View>
                    <Text />
                    <Text />
                    <Text />
                    <TouchableHighlight onPress={() => { this.props.reset() }}>
                        <Text>       Project Posted!  Click here to list a new Project.</Text>
                    </TouchableHighlight>
                    <Text />
                    <Text />
                    <Text>       (We are now working on pairing your project to a PenPal!)</Text>
                </View >
            </View>

        )
    }
}


const styles = StyleSheet.create({
    core: {
        backgroundColor: '#fff'
    },
    main: {
        marginTop: 10,
        // backgroundColor: '#fff'
    },
    container1: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    refresh: {
        // flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    flexer: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    }
})
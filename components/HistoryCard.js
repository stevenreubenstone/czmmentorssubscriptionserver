import React from 'react';
import {
    Image,
    Platform,
    ScrollView,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    TouchableHighlight,
    View,
} from 'react-native';


export default class HistoryCard extends React.Component {

    render() {
        return (
            <View style={styles.outline}>
                <Text>Project: {this.props.data.title}</Text>
                <Text></Text>
                {this.props.data.milestones.map(item => {
                    return <View>
                        <Text>Milestone: {item.body}</Text>
                        <Text />
                        <Text>PenPal: {item.pair.creator.First_Name} {item.pair.creator.Last_Name}</Text>
                        <Text></Text>
                        <Text>PenPal's Project: {item.pair.title}</Text>
                        <Text></Text>
                        <Text>PenPal Milestone: {item.pair.milestone.body}</Text>
                    </View>
                })}
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 15,
        backgroundColor: '#fff',
    },
    outline: {
        borderWidth: 1,
        borderRadius: 3,
        minHeight: 100,
        margin: 8,
        marginBottom: 4,
        borderColor: '#515151',
        backgroundColor: '#fff',
        padding: 5
    },
    container1: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    snippitc: {
        borderWidth: 1,
        borderRadius: 3,
        minHeight: 100,
        margin: 15,
        marginBottom: 4,
        borderColor: '#515151',
        backgroundColor: '#fff'
    },
    avatar: {
        height: 100,
        borderRadius: 500,
        borderWidth: 60,
        width: 100,
    },
    imageContainer: {
        flex: 1,
        alignItems: 'flex-end'
    },
    refresh: {
        // flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
});
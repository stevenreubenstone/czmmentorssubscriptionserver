import React from 'react';
import { ScrollView, StyleSheet, Image, View, Text, TouchableHighlight, TextInput, KeyboardAvoidingView } from 'react-native';
import { Button } from 'react-native-elements';
import { Mutation } from "react-apollo";
import { SEND_CHAT } from "../queries.js";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';


export default class ChatScreen extends React.Component {
    static navigationOptions = {
        title: 'Chat',
    };

    state = {
        body: ''
    }

    render() {
        const body = this.state.body
        const connection = this.props.connection
        return (



            <Mutation mutation={SEND_CHAT}>
                {sendChat => (
                    <View>
                        <View style={{ marginRight: 7, marginBottom: 4, flexDirection: "row", justifyContent: 'flex-end' }}>
                            <TouchableHighlight onPress={() => {
                                sendChat({ variables: { connection: connection, body: body } });
                                this.textInput.clear()
                            }}>
                                <Text> Send </Text>
                            </TouchableHighlight>
                        </View>
                        <TextInput
                            ref={input => { this.textInput = input }} style={{ height: 50, borderColor: 'gray', borderWidth: 1, borderRadius: 4, marginBottom: 1, paddingLeft: 5 }}
                            multiline={true} placeholder='type your chat' onChangeText={(text) => this.setState({ body: text })}
                        />
                    </View>
                )}
            </Mutation>



        )
    }
}


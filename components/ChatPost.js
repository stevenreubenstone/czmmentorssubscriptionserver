import React from 'react';
import { Mutation } from "react-apollo";
import { POST_ACTIVITY } from "../queries.js";
import { ScrollView, StyleSheet, Image, View, Text, TouchableHighlight } from 'react-native';

export default class ChatPost extends React.Component {

    render() {
        return (
            <Mutation mutation={POST_ACTIVITY}>
                {postActivity => (
                    <View>
                        <Text style={{ marginLeft: 10, marginTop: 30 }}>Are you sure you want to post this to the Activity Feed?</Text>
                        <Text />

                        <TouchableHighlight onPress={() => {
                            postActivity({ variables: { chat_id: this.props.chat } });
                            this.props.func(false, 1);
                        }}>
                            <Text style={{ marginLeft: 10, fontSize: 16 }}>Yes</Text>
                        </TouchableHighlight>
                        <Text />
                    </View>
                )}
            </Mutation>
        )
    };
}


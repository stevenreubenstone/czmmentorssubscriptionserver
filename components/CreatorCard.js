import React from 'react';
import {
    Image,
    Picker,
    Platform,
    ScrollView,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View,
} from 'react-native';
import { CheckBox } from 'react-native-elements'
import { Mutation } from "react-apollo";
import { UPDATE_SNIPPIT } from "../queries.js";
import { Button } from 'react-native-elements';

export default class CreatorCard extends React.Component {

    render() {

        return (
            <View>
                <View style={styles.snippitc}>
                    <Text> Snippit: {this.props.datac.title}</Text>
                    <View
                        style={{
                            borderBottomColor: '#515151',
                            borderBottomWidth: .5,
                        }}
                    />
                    <Text style={{ fontWeight: 'bold', paddingTop: 6 }}> Milestones</Text>
                    <Text />
                    <Text style={{ marginLeft: 3 }}>{this.props.datac.milestones}</Text>
                    <Text />
                    <Text style={{ fontWeight: 'bold' }}> Funding Steps To Go </Text>
                    <Text />
                    <Text style={{ marginLeft: 3 }}>{this.props.datac.to_funding}</Text>
                </View>
                <Text style={{ fontSize: 11, marginLeft: 15 }}>Mentor: {(!this.props.datac.mentor) ? 'No mentor assigned yet.' : `${this.props.datac.mentor.First_Name} ${this.props.datac.mentor.Last_Name}`}</Text>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 15,
        backgroundColor: '#fff',
    },
    flexPicker: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-start'
    },
    container1: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    snippitc: {
        borderWidth: 1,
        borderRadius: 3,
        minHeight: 100,
        margin: 15,
        marginBottom: 4,
        borderColor: '#515151',
        backgroundColor: '#fff'
    },
    avatar: {
        height: 100,
        borderRadius: 500,
        borderWidth: 60,
        width: 100,
    },
    imageContainer: {
        flex: 1,
        alignItems: 'flex-end'
    }
});

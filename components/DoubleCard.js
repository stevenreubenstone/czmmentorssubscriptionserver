import React from 'react';
import { ScrollView, StyleSheet, Image, View, Text, TouchableHighlight } from 'react-native';

export default class DoubleCard extends React.Component {

    render() {
        return (
            <TouchableHighlight onPress={() => { this.props.func(this.props.data.connection.id) }}>
                <View>
                    <View style={styles.containerY}>
                        <Image
                            style={{ width: 70, height: 70, borderRadius: 35, marginLeft: 7, marginTop: 9, marginBottom: 9 }}
                            source={{ uri: `${this.props.data.pair.creator.user_avatar}` }}
                        />
                        <View style={{ marginLeft: 10, marginRight: 85, marginTop: 13, marginBottom: 9 }}>
                            <Text style={{ fontSize: 11, paddingBottom: 4 }}>{`${this.props.data.pair.creator.First_Name} ${this.props.data.pair.creator.Last_Name}`}</Text>
                            <Text style={{ fontSize: 11, paddingBottom: 4 }}>{this.props.data.pair.creator.First_Name}'s Project: {this.props.data.pair.title}</Text>
                            <Text style={{ fontSize: 11, paddingBottom: 4 }}>{this.props.data.pair.creator.First_Name}'s Mini-Milestone: {(!this.props.data.pair.milestone.body) ? 'You must now help your PenPal generate a meaningful Milestone. Enter the chat room to learn more.' : `${this.props.data.pair.milestone.body}`}</Text>
                            {
                                (this.props.data.pair.milestone.completed === false) ? null : <Text style={{ fontSize: 8 }}>This mini-milestone has been completed.</Text>
                            }

                        </View>

                        <View style={{ borderBottomWidth: 1, margin: 15, marginBottom: 0, marginTop: 30 }} />
                    </View>
                    <View style={{ borderWidth: .5, borderRadius: 0, borderColor: '#E4E4E5', marginLeft: 0, marginRight: 0, marginBottom: 15 }} >
                        <Text style={{ fontSize: 10, paddingBottom: 4, marginLeft: 10, marginRight: 10, marginTop: 5 }}>My Project: {this.props.data.title}</Text>
                        <Text style={{ fontSize: 10, marginLeft: 10, marginRight: 10, paddingBottom: 4 }}>My Mini-Milestone: {(!this.props.data.milestone.body) ? 'You must work with your PenPal to generate a new Milestone. Enter the chat room to learn more.' : `${this.props.data.milestone.body}`}</Text>
                        {
                            (this.props.data.milestone.completed === false) ? null : <Text style={{ fontSize: 9 }}>This mini-milestone has been completed.</Text>
                        }
                    </View>
                    <View style={styles.container1}>
                        <Text style={{ fontSize: 25, marginBottom: 15, color: '#b0b0b0' }}>...</Text>
                    </View>
                </View>

            </TouchableHighlight>


        )
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 15,
        backgroundColor: '#fff',
    },
    container1: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    containerY: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        borderBottomWidth: 1,
        borderTopWidth: 1,
        borderColor: '#E4E4E5'
    },
    snippitc: {
        borderWidth: 1,
        borderRadius: 3,
        minHeight: 100,
        margin: 15,
        marginBottom: 4,
        borderColor: '#515151'
    },
    avatar: {
        height: 100,
        borderRadius: 500,
        borderWidth: 60,
        width: 100,
    },
    imageContainer: {
        flex: 1,
        alignItems: 'flex-end'
    },
    refresh: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end'
    }
});






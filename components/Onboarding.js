import { Image } from 'react-native';
import React from 'react';
import { Mutation } from "react-apollo";
import { ONBOARD } from "../queries.js";

import Onboarding from 'react-native-onboarding-swiper';

const Onboard = (props) => (
    <Mutation mutation={ONBOARD}>
        {onBoard => (
            <Onboarding
                pages={[
                    {
                        backgroundColor: '#fff',
                        image: <Image source={require('../assets/images/alone.png')} style={{ width: 330, height: 132 }} />,
                        title: 'Welcome to Collaborizm PenPals.',
                        subtitle: '4 Seconds to Learn How it Works.',
                    },
                    {
                        backgroundColor: '#fff',
                        image: <Image source={require('../assets/images/project.png')} style={{ width: 100, height: 100 }} />,
                        title: 'Launch a Project',
                        subtitle: `Provide a title and short description of any entrepreneurial or passion Project you're currently working on.`,
                    },
                    {
                        backgroundColor: '#fff',
                        image: <Image source={require('../assets/images/magnet.png')} style={{ width: 150, height: 150 }} />,
                        title: 'Get Matched to a PenPal.',
                        subtitle: `The PenPal matching algorithm finds you a great match.`,
                    },
                    {
                        backgroundColor: '#fff',
                        image: <Image source={require('../assets/images/profit.png')} style={{ width: 200, height: 200 }} />,
                        title: 'Help Each Other Make Progress',
                        subtitle: "Help your PenPal with their Mini-Milestone. Then move to a new PenPal.",
                    },
                ]}

                onDone={() => {
                    onBoard();
                    props.refetch();
                }
                }

                showSkip={false}
            />
        )}

    </Mutation>
);

export default Onboard;
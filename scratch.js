<View>
    <View style={styles.snippitc}>
        <Text style={{ fontSize: 12, fontWeight: 'bold', paddingTop: 10, paddingLeft: 4, color: '#535252' }}>My Project: {this.props.data.title}</Text>
        <Text style={{ fontSize: 12, padding: 5, color: '#535252' }}>{this.props.data.body}</Text>
        <Text />
        <Text style={{ fontSize: 12, fontWeight: 'bold', paddingTop: 10, paddingLeft: 4, color: '#535252' }}>{this.props.data.creator.First_Name}'s Milestone</Text>
        <Text style={{ fontSize: 12, padding: 5, color: '#535252' }}>{(!this.props.data.milestone.body) ? 'You must work with your PenPal to generate a new Milestone. Enter the chat room to learn more.' : `${this.props.data.milestone.body}`}</Text>
        {
            (this.props.data.milestone.completed === false) ? null : <Text>This milestone has been compelted.</Text>
        }
        <Text />
        <View style={{ borderBottomColor: '#515151', borderBottomWidth: .5 }} />
        <Text style={{ fontSize: 10, padding: 5, color: '#535252' }}> Creator: {`${this.props.data.creator.First_Name} ${this.props.data.creator.Last_Name}`}</Text>
        <Text style={{ fontSize: 10, padding: 5, paddingTop: 1, color: '#535252' }}> PenPal: {(!this.props.data.pair) ? 'No PenPal matched yet. Check back later.' : `${this.props.data.pair.creator.First_Name} ${this.props.data.pair.creator.Last_Name}`}</Text>
    </View>

    <View>
        <View style={styles.containerY}>
            <Image
                style={{ width: 40, height: 40, borderRadius: 20, marginTop: 13, marginLeft: 127 }}
                source={{ uri: `${this.props.data.creator.user_avatar}` }}
            />
            <Image source={require('../assets/images/arrows.png')} style={{ marginTop: 20, width: 20, height: 20 }} />
            <Image
                style={{ width: 40, height: 40, borderRadius: 20, marginTop: 13, marginRight: 127 }}
                source={{ uri: `${this.props.data.pair.creator.user_avatar}` }}
            />
        </View>

        <View style={styles.snippitc}>
            <Text style={{ fontSize: 12, fontWeight: 'bold', paddingTop: 10, paddingLeft: 4, color: '#535252' }}>PenPal's Project: {this.props.data.pair.title}</Text>
            <Text style={{ fontSize: 12, padding: 5, color: '#535252' }}>{this.props.data.pair.body}</Text>
            <Text />
            <Text style={{ fontSize: 12, fontWeight: 'bold', paddingTop: 10, paddingLeft: 4, color: '#535252' }}>{this.props.data.pair.creator.First_Name}'s Milestone</Text>
            <Text style={{ fontSize: 12, padding: 5, color: '#535252' }}>{(!this.props.data.pair.milestone.body) ? 'You must now help your PenPal generate a meaningful Milestone. Enter the chat room to learn more.' : `${this.props.data.pair.milestone.body}`}</Text>
            {
                (this.props.data.pair.milestone.completed === false) ? null : <Text>This milestone has been compelted.</Text>
            }
            <Text />
            <View style={{ borderBottomColor: '#515151', borderBottomWidth: .5 }} />
            <Text style={{ fontSize: 10, padding: 5, color: '#535252' }}> Creator: {`${this.props.data.pair.creator.First_Name} ${this.props.data.pair.creator.Last_Name}`}</Text>
            <Text style={{ fontSize: 10, padding: 5, paddingTop: 1, color: '#535252' }}> PenPal: {`${this.props.data.creator.First_Name} ${this.props.data.creator.Last_Name}`}</Text>
        </View>
    </View>
</View> 
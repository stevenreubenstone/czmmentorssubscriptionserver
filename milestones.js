



const a = "Better explain my project's Problem Statement."

const b = "Improve my elevator pitch."

const c = "Have a discussion about my ideal customer persona."

const d = "Have a conversation about the best coffee to drink as an entrepreneur."

const e = "Discuss the most difficult problem I am facing now."

const f = "Discuss the luckiest thing that's every happened to me."

const g = "Discuss how my family influences my entrepreneurial/creative drive."

const h = "Discuss the dream outcome for my project."

const i = "Discuss how I get myself into my optimal state of creative rhythm."

const j = "Make a small improvement on my marketing strategy."

const milestones = [a, b, c, d, e, f, g, h, i, j]



export default milestones
import React from 'react';
import { ApolloProvider } from "react-apollo";
import { setContext } from 'apollo-link-context';
import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-link-http';
import { onError } from 'apollo-link-error';
import { ApolloLink } from 'apollo-link';
import { WebSocketLink } from 'apollo-link-ws';
import { Query } from "react-apollo";
import { AUTH } from "./queries.js";
import { Platform, StatusBar, StyleSheet, View, Text } from 'react-native';
import { AppLoading, Asset, Font, Icon } from 'expo';
import AppNavigator from './navigation/AppNavigator';
import Login from './components/Login';
import { split } from 'apollo-link';
import { getMainDefinition } from 'apollo-utilities';
import Onboarder from './components/Onboarding';

// PRODUCTION
// const httpLink = new HttpLink({
//   uri: "https://mentor-backend.herokuapp.com/graphql",
//   credentials: 'include'
// });

// LOCAL
const httpLink = new HttpLink({
  uri: "https://perfect-dragonfly-46.localtunnel.me/graphql",
  credentials: 'include'
});


const authLink = setContext(async (_, { headers }) => {
  const toker = await Expo.SecureStore.getItemAsync("fbToken");
  console.log('toker here', toker)
  return {
    headers: {
      ...headers,
      fbtoken: toker ? `${toker}` : ""
    }
  }
});

// PRODUCTION

// const wsLink = new WebSocketLink({
//   uri: `ws://mentor-socket-backend.herokuapp.com/subscriptions`,
//   options: {
//     reconnect: true
//   }
// });

// LOCAL
const wsLink = new WebSocketLink({
  uri: `ws://polite-lion-68.localtunnel.me/subscriptions`,
  options: {
    reconnect: true
  }
});




const link = split(
  // split based on operation type
  ({ query }) => {
    const { kind, operation } = getMainDefinition(query);
    return kind === 'OperationDefinition' && operation === 'subscription';
  },
  wsLink,
  authLink.concat(httpLink),
);


const client = new ApolloClient({
  link,
  cache: new InMemoryCache()
});

export default class App extends React.Component {
  state = {
    isLoadingComplete: false,
    onboarded: false,
    // user: undefined,
  };

  clearSecureStore = async () => {
    const result = await Expo.SecureStore.deleteItemAsync("fbToken")
    return null
  }

  done = () => {
    this.setState({ onboarded: true })
  }

  // componentDidMount = () => {
  //   this.clearSecureStore();
  // }

  render() {


    if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
      return (
        <AppLoading
          startAsync={this._loadResourcesAsync}
          onError={this._handleLoadingError}
          onFinish={this._handleFinishLoading}
        />
      );
    } else {
      return (
        <ApolloProvider client={client}>
          <Query query={AUTH} fetchPolicy="network-only">
            {({ loading, error, data, refetch }) => {
              if (loading) return <Text>Loading...</Text>;
              if (error) return <Text>There is error with getAuth</Text>;
              console.log('OFFICIAL AUTH---', data)
              return (
                (data.getAuth.id === null) ? <Login refetch={refetch} /> :
                  (data.getAuth.onboarded === false) ? <Onboarder refetch={refetch} /> :
                    <View style={styles.container}>
                      {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
                      <AppNavigator />
                    </View>
              )
            }}
          </Query>
        </ApolloProvider>
      );
    }
  }

  _loadResourcesAsync = async () => {
    return Promise.all([
      Asset.loadAsync([
        require('./assets/images/robot-dev.png'),
        require('./assets/images/robot-prod.png'),
      ]),
      Font.loadAsync({
        // This is the font that we are using for our tab bar
        ...Icon.Ionicons.font,
        // We include SpaceMono because we use it in HomeScreen.js. Feel free
        // to remove this if you are not using it in your app
        'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf'),
      }),
    ]);
  };

  _handleLoadingError = error => {
    // In this case, you might want to report the error to your error
    // reporting service, for example Sentry
    console.warn(error);
  };

  _handleFinishLoading = () => {
    this.setState({ isLoadingComplete: true });
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});

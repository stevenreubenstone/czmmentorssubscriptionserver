import { gql } from "apollo-boost";

export const CREATE_SNIPPIT = gql`
  mutation createSnippit($input: snippitInput) {
    createSnippit(input: $input) {
      id
    }
  }
`;

export const SEND_CHAT = gql`
  mutation sendChat($connection: Int!, $body: String!) {
    sendChat(connection: $connection, body: $body) {
      id
    }
  }
`;

export const CHECK_MILESTONE = gql`
  mutation checkMilestone($milestone_id: Int!, $mymilestone: Int!) {
    checkMilestone(milestone_id: $milestone_id, mymilestone: $mymilestone) {
      id
    }
  }
`;

export const POST_ACTIVITY = gql`
  mutation postActivity($chat_id: Int!) {
    postActivity(chat_id: $chat_id) {
      id
    }
  }
`;

export const UPDATE_MILESTONE = gql`
  mutation updateMilestone($milestone: String, $milestone_id: Int) {
    updateMilestone(milestone: $milestone, milestone_id: $milestone_id) {
      id
  }
}
`;


export const ARCHIVE_SNIPPIT = gql`
  mutation archiveSnippit($id: Int!) {
    archiveSnippit(id: $id) {
      id
    }
  }
`;

export const PUSH_TOKEN = gql`
  mutation set_pushToken ($push_token: String!) {
    set_pushToken(push_token: $push_token) {
      id
    }
  }
`;

export const SKIP = gql`
  mutation skip ($snippit_id: Int!) {
    skip(snippit_id: $snippit_id) {
      id
    }
  }
`;

export const AUTH = gql`{
  getAuth {
   id
   First_Name
   Last_Name
   onboarded
  }
}
`;

export const ONBOARD = gql`
  mutation { 
    onBoard {
      id
    }
  }
`;

export const HISTORY = gql`{
  getUserSnippits {
    title
    milestones {
      body
      pair {
        creator {
          First_Name
          Last_Name
        }
        title
        milestone {
          body
        }
      }
     }
    }
}
`;

export const GET_USER_SNIPPITS = gql`{
   getUserSnippits {
      id
      title
      milestones {
      body
      pair {
        creator {
          First_Name
          Last_Name
        }
        title
        milestone {
          body
        }
      }
     }
      body
      connection {
        id
      }
      creator {
        First_Name
        Last_Name
        user_avatar
      }
      pair {
        title
        body
        creator {
          First_Name
          Last_Name
          user_avatar
        }
        milestone {
          id
          body
          completed
        }
    }
    milestone {
      id
      project_id
      body
      project_match
      completed
    }
  }
}
`;



export const GET_CHAT = gql`
  query getChat($connection: Int!) {
    getChat(connection: $connection) {
      id
      body
      user {
        First_Name
        Last_Name
        id
        user_avatar
      }
  }
}
`;






export const GET_ACTIVITY = gql`{
  getActivity {
    user_id
    title
    body
    snippit {
      title
      creator {
        id
        user_avatar
      }
      mentor {
        id
        First_Name
        Last_Name
        user_avatar
      }
    }
  }
}
`;



export const SUBSCRIBE_CHAT = gql`
  subscription chatSent($connection: Int!) {
    chatSent (connection: $connection) {
      id
      body
      user {
        First_Name
        Last_Name
        id
        user_avatar
      }
    }
}
`;



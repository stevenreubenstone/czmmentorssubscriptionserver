import React from 'react';
import { ApolloConsumer } from "react-apollo";
import Push from '../components/Push';
import About from '../components/About';
import Tip from '../components/Tip';
import { Mutation } from "react-apollo";
import { CREATE_SNIPPIT, GET_USER_SNIPPITS } from "../queries";
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Keyboard,
  Text,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  TouchableHighlight,
  KeyboardAvoidingView,
  View,
} from 'react-native';
import { Button } from 'react-native-elements';
import Success from '../components/SubmitSuccess';

export default class HomeScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Launch a Project',
      // headerRight: (
      //   <TouchableHighlight onPress={navigation.getParam('about')}>
      //     <Text style={{ marginRight: 10 }}>How it Works</Text>
      //   </TouchableHighlight>
      // ),
    };
  }

  state = {
    title: '',
    body: '',
    milestone1: '',
    submitted: false,
    about: false,
    tip: false,
  }

  componentDidMount() {
    this.props.navigation.setParams({ about: this.toAbout });
  }

  reset = () => {
    this.setState({ submitted: false })
  }

  toAbout = () => {
    this.setState({ about: !this.state.about })
  }

  toTip = () => {
    this.setState({ tip: !this.state.tip })
  }


  render() {
    const title = this.state.title
    const body = this.state.body
    const milestone1 = {
      body: this.state.milestone1
    }

    const input = {
      title: title,
      body: body,
      milestones: [milestone1]
    }



    return (
      <KeyboardAvoidingView style={{ flex: 1, backgroundColor: '#fff' }} behavior="padding" enabled>
        <ApolloConsumer>
          {client => <Push client={client} />}
        </ApolloConsumer>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
          <ScrollView style={styles.core}>
            {(this.state.tip === true) ? <Tip func={this.toTip} /> :
              (this.state.about === true) ? <About func={this.toAbout} /> :
                (this.state.submitted === false) ? <Mutation mutation={CREATE_SNIPPIT}>
                  {createSnippit => (
                    <View style={styles.main}>
                      {/* <View style={styles.refresh}>
                        <TouchableHighlight onPress={() => this.toAbout()}>
                          <Text style={{ fontSize: 10, marginRight: 8, color: '#2A3DA8' }}>How it Works</Text>
                        </TouchableHighlight>
                      </View> */}
                      <View style={styles.container1}>
                        <Image source={require('../assets/images/project.png')} style={{ marginTop: 23, width: 65, height: 65 }} />
                      </View>
                      <View style={styles.container1}>
                        <Text style={{ fontSize: 8, marginTop: 5 }}>You are logged in.</Text>
                      </View>
                      <View>
                        {/* <Text style={{ marginLeft: 10, marginTop: 30, fontSize: 15 }}>Launch a New Project.</Text>
                        <Text style={{ marginLeft: 10, marginTop: 10, fontSize: 13 }}>(1) Enter a title for your Project. </Text> */}
                        <TextInput
                          style={{ height: 40, borderColor: 'gray', borderWidth: 1, borderRadius: 4, margin: 10, marginTop: 30, height: 30, paddingLeft: 5 }}
                          multiline={true} placeholder='Project Title' onChangeText={(text) => this.setState({ title: text })}
                        />

                        {/* <Text style={{ marginLeft: 10, marginTop: 10, fontSize: 13 }}>(2) Provide a few sentences for your Project Description.</Text> */}


                        <TextInput
                          style={{ height: 40, borderColor: 'gray', borderWidth: 1, borderRadius: 4, margin: 10, height: 200, paddingLeft: 5 }}
                          multiline={true} placeholder='Project Description' onChangeText={(text) => this.setState({ body: text })}
                        />


                        <View style={styles.container1}>
                          <Button
                            onPress={() => {
                              createSnippit({ variables: { input: input } });
                              this.setState({ submitted: true });
                            }}
                            title="Launch Project"
                            buttonStyle={{
                              backgroundColor: "rgba(255, 255, 255, 1)",
                              width: 165,
                              height: 44,
                              borderColor: "#676767",
                              borderWidth: 0.5,
                              borderRadius: 5,
                            }}
                            color="#841584"
                            accessibilityLabel="Learn more about this purple button"
                          />



                        </View>
                        <View style={styles.container1}>
                          <TouchableHighlight onPress={() => this.toAbout()} >
                            <Text style={{ marginTop: 25, fontSize: 12 }}>or learn more about how it works.</Text>
                          </TouchableHighlight>
                        </View>

                      </View>
                    </View>
                  )}
                </Mutation> : <Success styler={styles} reset={this.reset} />}
          </ScrollView>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    );
  }

}

const styles = StyleSheet.create({
  core: {
    backgroundColor: '#fff'
  },
  main: {
    marginTop: 10,
    // backgroundColor: '#fff'
  },
  container1: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center'
  },
  refresh: {
    // flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  flexer: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  }
})
import React from 'react';
import { Query } from "react-apollo";
import { GET_USER_SNIPPITS } from "../queries.js";
import ProgressControl from '../components/ProgressControl';
import History from '../components/History';
import Tip from '../components/Tip';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  TouchableHighlight,
  View,
} from 'react-native';
import { Button } from 'react-native-elements';

export default class SettingsScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Milestones',
      headerRight: (
        <TouchableHighlight onPress={navigation.getParam('history')}>
          <Text style={{ marginRight: 10 }}>History</Text>
        </TouchableHighlight>
      ),
      // headerLeft: (
      //   <TouchableHighlight onPress={navigation.getParam('history')}>
      //     <Text style={{ marginLeft: 10 }}>refresh</Text>
      //   </TouchableHighlight>
      // ),
    };
  }

  state = {
    tip: false,
    history: false
  }

  componentDidMount() {
    this.props.navigation.setParams({ history: this._history });
  }

  _history = () => {
    this.setState({ history: !this.state.history })
  }

  toTip = () => {
    this.setState({ tip: !this.state.tip })
  }


  render() {
    return (
      <Query query={GET_USER_SNIPPITS} fetchPolicy="cache-and-network">
        {({ loading, error, data, refetch }) => {
          if (loading) return <Text>Loading...</Text>;
          if (error) return <Text>There is error</Text>;
          return (
            (this.state.history === true) ? <History data={data} /> :
              (this.state.tip === false) ?
                <ScrollView>
                  <View style={styles.refresh}>
                    <TouchableHighlight onPress={() => refetch()}>
                      <Text style={{ color: '#39ACF4', fontSize: 9, marginRight: 12, marginTop: 8 }}>refresh</Text>
                    </TouchableHighlight>
                  </View>



                  <View style={styles.container1}>
                    <Image source={require('../assets/images/profit.png')} style={{ marginTop: 5, marginBottom: 30, width: 50, height: 50 }} />
                  </View>
                  <View style={styles.container1}>
                    <Text style={{ fontSize: 16, marginBottom: 2 }}>Milestones by PenPal</Text>
                  </View>
                  <View style={styles.container1}>
                    <Text style={{ fontSize: 11, marginBottom: 12 }}>Each card represents a PenPal match.</Text>
                  </View>
                  <View style={{ borderBottomWidth: .5, marginRight: 20, marginLeft: 20, marginBottom: 15 }} />
                  <ProgressControl data={data} tip={this.toTip} />
                  <View style={styles.outline}>
                    <Text style={{ margin: 8, marginRight: 13, marginBottom: 0, fontWeight: 'bold' }}>What are Milestones?</Text>
                    <Text style={{ margin: 8, marginTop: 4, marginRight: 13 }}>Milestones = Mini-Milestones! Mini-Milestones are small tasks for you to work on with your PenPal. Mini-Milestones should help you push your project forward, and also create a canvas to learn more about your PenPal. The Mini-Milestone Library will help you select great mini-milestones.</Text>
                  </View>
                </ScrollView>
                : <Tip func={this.toTip} />
          )
        }}
      </Query>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
  container1: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center'
  },
  outline: {
    // borderWidth: 1,
    borderRadius: 3,
    minHeight: 50,
    width: 400,
    margin: 8,
    marginBottom: 4,
    borderColor: '#515151',
    backgroundColor: '#fff',
    marginBottom: 20
  },
  snippitc: {
    borderWidth: 1,
    borderRadius: 3,
    minHeight: 100,
    margin: 15,
    marginBottom: 4,
    borderColor: '#515151',
    backgroundColor: '#fff'
  },
  avatar: {
    height: 100,
    borderRadius: 500,
    borderWidth: 60,
    width: 100,
  },
  imageContainer: {
    flex: 1,
    alignItems: 'flex-end'
  },
  refresh: {
    // flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
});
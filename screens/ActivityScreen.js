import React from 'react';
import { ApolloConsumer } from "react-apollo";
import Push from '../components/Push';
import { Query } from "react-apollo";
import { GET_ACTIVITY } from "../queries";
import {
    Image,
    TouchableHighlight,
    Platform,
    ScrollView,
    StyleSheet,
    Keyboard,
    Text,
    TextInput,
    TouchableOpacity,
    TouchableWithoutFeedback,
    KeyboardAvoidingView,
    View,
} from 'react-native';
import { Button } from 'react-native-elements';
import Success from '../components/SubmitSuccess';

export default class ActivityScreen extends React.Component {
    static navigationOptions = {
        header: null,
    };

    render() {
        return (
            <Query query={GET_ACTIVITY} fetchPolicy="network-only">
                {({ loading, error, data, refetch }) => {
                    if (loading) return <Text>Loading...</Text>;
                    if (error) return <Text>There is error</Text>;
                    return (
                        <ScrollView style={styles.container}>
                            <View style={styles.refresh}>
                                <TouchableHighlight onPress={() => refetch()}>
                                    <Text style={{ fontSize: 10, marginRight: 8, marginTop: 4 }}>Refresh Feed</Text>
                                </TouchableHighlight>
                            </View>
                            <View style={styles.container1}>
                                <Image source={require('../assets/images/Rexclear.png')} style={{ marginTop: 20, width: 64, height: 64 }} />
                            </View>
                            <View style={styles.container1}>
                                <Text style={{ marginTop: 10, marginBottom: 17, fontWeight: "bold", color: '#535252' }}>Collaborizm Mentors: Activity Feed</Text>
                            </View>
                            {data.getActivity.map((item) => (
                                <View style={{ marginBottom: 15 }}>
                                    <View style={styles.snippitc}>
                                        <View style={styles.container5}>
                                            <View>
                                                <Image
                                                    style={{ width: 24, height: 24, borderRadius: 12, marginLeft: 7, marginTop: 7 }}
                                                    source={item.user_id === item.snippit.creator.id ? { uri: `${item.snippit.creator.user_avatar}` } : { uri: `${item.snippit.mentor.user_avatar}` }}
                                                />
                                            </View>
                                            <View>
                                                <Text style={{ fontSize: 12, fontWeight: 'bold', paddingTop: 5, paddingLeft: 4, color: '#535252' }}>{item.title}</Text>
                                                <Text style={{ fontSize: 12, padding: 5, paddingBottom: 18, color: '#535252' }}>{item.body}</Text>
                                            </View>
                                        </View>
                                        <View style={{ borderBottomColor: '#515151', borderBottomWidth: .5 }} />
                                        <Text style={{ fontSize: 10, padding: 5, paddingBottom: 1, color: '#535252' }}>Snippit: {item.snippit.title}</Text>
                                        <Text style={{ fontSize: 10, padding: 5, color: '#535252' }}>Mentor: {(item.snippit.mentor === null) ? null : item.snippit.mentor.First_Name} {(item.snippit.mentor === null) ? "No mentor yet." : item.snippit.mentor.Last_Name}</Text>
                                    </View>
                                </View>
                            ))}

                        </ScrollView>
                    )
                }}
            </Query>
        )

    }
}

const styles = StyleSheet.create({
    container5: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        marginRight: 10
    },
    container: {
        flex: 1,
        paddingTop: 15,
        backgroundColor: '#fff',
    },
    container1: {
        //flex: 1,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    snippitc: {
        borderWidth: 1,
        borderRadius: 3,
        minHeight: 100,
        margin: 15,
        marginTop: 5,
        marginBottom: 4,
        borderColor: '#515151'
    },
    avatar: {
        height: 100,
        borderRadius: 500,
        borderWidth: 60,
        width: 100,
    },
    imageContainer: {
        flex: 1,
        alignItems: 'flex-end'
    },
    refresh: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end'
    }
});
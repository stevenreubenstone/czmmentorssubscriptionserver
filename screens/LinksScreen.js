import React from 'react';
import ChatControl from '../components/ChatControl';
import { Query } from "react-apollo";
import { GET_USER_SNIPPITS } from "../queries.js";
import { ScrollView, StyleSheet, Image, View, Text, TouchableHighlight } from 'react-native';
import Archive from '../components/Archive';
import About from '../components/About';
import DoubleCard from '../components/DoubleCard';
import SingleCard from '../components/SingleCard';
import Nomatch from '../components/Nomatch';

export default class LinksScreen extends React.Component {
  static navigationOptions = {
    title: 'My PenPals',
  };

  state = {
    chat_id: null,
    chat: false,
    about: false,
  }

  toThread = (id) => {
    this.setState({ chat: !this.state.chat, chat_id: id })
  }

  toAbout = () => {
    this.setState({ about: !this.state.about })
  }

  render() {
    return (
      <Query query={GET_USER_SNIPPITS} fetchPolicy="network-only">
        {({ loading, error, data, refetch }) => {
          if (loading) return <Text>Loading...</Text>;
          if (error) return <Text>There is error in GraphQL Query</Text>;
          const array = data.getUserSnippits.filter(item => {
            return item.connection
          })
          return (
            (this.state.about === true) ? <About func={this.toAbout} /> :
              (this.state.chat === false) ?
                <ScrollView style={styles.container}>
                  <View>
                    <View style={styles.refresh}>
                      <TouchableHighlight onPress={() => refetch()}>
                        <Text style={{ fontSize: 10, marginRight: 8 }}>Refresh PenPals</Text>
                      </TouchableHighlight>
                    </View>
                    <View style={styles.container1}>
                      <Image source={require('../assets/images/storm.png')} style={{ marginTop: 12, marginBottom: 20, width: 50, height: 50 }} />
                    </View>
                    <Text style={{ marginTop: 8, marginLeft: 15, marginBottom: 1, fontWeight: 'bold' }}>PenPal Matches</Text>
                    <Text style={{ marginTop: 0, marginLeft: 15, marginBottom: 15, fontSize: 12 }}>(Click on a PenPal to chat.)</Text>
                    {(array.length === 0) ? <Nomatch /> :

                      data.getUserSnippits.map((item) => {
                        if (item.connection === null) {
                          return null
                        }
                        else {
                          return <DoubleCard data={item} func={this.toThread} />
                        }

                      })}

                    <View style={{ borderBottomWidth: 1, margin: 15, marginBottom: 0, marginTop: 130, borderColor: '#E4E4E5' }} />
                    <Text style={{ marginTop: 8, marginLeft: 15, fontWeight: 'bold', color: '#535252' }}>Projects Waiting for PenPals</Text>
                    <Text style={{ fontSize: 12, marginTop: 8, marginLeft: 15, color: '#535252' }}>Projects that have not matched yet will show up here.</Text>


                    {data.getUserSnippits.map((item) => {
                      if (item.connection !== null) {
                        return null
                      }
                      else {
                        return <SingleCard data={item} />
                      }
                    })}

                  </View>
                </ScrollView>
                : <ChatControl reset={this.toThread} connection={this.state.chat_id} />
          )
        }}
      </Query>
    )
  };
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
  container1: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center'
  },
  snippitc: {
    borderWidth: 1,
    borderRadius: 3,
    minHeight: 80,
    margin: 15,
    marginBottom: 4,
    borderColor: '#515151'
  },
  avatar: {
    height: 100,
    borderRadius: 500,
    borderWidth: 60,
    width: 100,
  },
  imageContainer: {
    flex: 1,
    alignItems: 'flex-end'
  },
  refresh: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end'
  }
});



// break

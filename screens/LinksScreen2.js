
import React from 'react';
import ChatControl from '../components/ChatControl';
import { Query } from "react-apollo";
import { GET_USER_SNIPPITS } from "../queries.js";
import { ScrollView, StyleSheet, Image, View, Text, TouchableHighlight } from 'react-native';
import Archive from '../components/Archive';
import About from '../components/About';


export default class LinksScreen extends React.Component {
    static navigationOptions = {
        title: 'My PenPals',
    };

    state = {
        chat_id: null,
        chat: false,
        about: false,
    }

    toThread = (id) => {
        this.setState({ chat: !this.state.chat, chat_id: id })
    }

    toAbout = () => {
        this.setState({ about: !this.state.about })
    }

    render() {
        return (
            <Query query={GET_USER_SNIPPITS} fetchPolicy="network-only">
                {({ loading, error, data, refetch }) => {
                    if (loading) return <Text>Loading...</Text>;
                    if (error) return <Text>There is error in GraphQL Query</Text>;
                    return (
                        (this.state.about === true) ? <About func={this.toAbout} /> :
                            (this.state.chat === false) ?
                                <ScrollView style={styles.container}>
                                    <View>
                                        <View style={styles.refresh}>
                                            <TouchableHighlight onPress={() => refetch()}>
                                                <Text style={{ fontSize: 10, marginRight: 8 }}>Refresh PenPals</Text>
                                            </TouchableHighlight>
                                        </View>
                                        <View style={styles.container1}>
                                            <Image source={require('../assets/images/storm.png')} style={{ marginTop: 12, width: 50, height: 50 }} />
                                        </View>

                                        <Text style={{ marginLeft: 15, fontWeight: 'bold' }}>Active PenPal Matches</Text>

                                        {data.getUserSnippits.map((item) => (
                                            (item.connection !== null) ?
                                                <TouchableHighlight onPress={() => { this.toThread(item.connection.id) }}>
                                                    <View>
                                                        <View style={styles.snippitc}>
                                                            <Text style={{ fontSize: 12, fontWeight: 'bold', paddingTop: 10, paddingLeft: 4, color: '#535252' }}>My Project: {item.title}</Text>
                                                            <Text style={{ fontSize: 12, padding: 5, color: '#535252' }}>{item.body}</Text>
                                                            <Text />
                                                            <Text style={{ fontSize: 12, fontWeight: 'bold', paddingTop: 10, paddingLeft: 4, color: '#535252' }}>Active Milestone</Text>
                                                            <Text style={{ fontSize: 12, padding: 5, color: '#535252' }}>{(!item.milestone.body) ? 'You must work with your PenPal to generate a new Milestone. Enter the chat room to learn more.' : `${item.milestone.body}`}</Text>
                                                            {
                                                                (item.milestone.completed === false) ? null : <Text>This milestone has been compelted.</Text>
                                                            }


                                                            <Text />
                                                            <View style={{ borderBottomColor: '#515151', borderBottomWidth: .5 }} />
                                                            <Text style={{ fontSize: 10, padding: 5, color: '#535252' }}> Creator: {`${item.creator.First_Name} ${item.creator.Last_Name}`}</Text>
                                                            <Text style={{ fontSize: 10, padding: 5, paddingTop: 1, color: '#535252' }}> PenPal: {(!item.pair) ? 'No PenPal matched yet. Check back later.' : `${item.pair.creator.First_Name} ${item.pair.creator.Last_Name}`}</Text>
                                                        </View>

                                                        {(item.pair != null) ?
                                                            <View>
                                                                <View style={styles.container1}>
                                                                    <Image source={require('../assets/images/arrows.png')} style={{ marginTop: 20, width: 20, height: 20 }} />
                                                                </View>
                                                                <View style={styles.snippitc}>
                                                                    <Text style={{ fontSize: 12, fontWeight: 'bold', paddingTop: 10, paddingLeft: 4, color: '#535252' }}>PenPal's Project: {item.pair.title}</Text>
                                                                    <Text style={{ fontSize: 12, padding: 5, color: '#535252' }}>{item.pair.body}</Text>
                                                                    <Text />
                                                                    <Text style={{ fontSize: 12, fontWeight: 'bold', paddingTop: 10, paddingLeft: 4, color: '#535252' }}>PenPal's Active Milestone</Text>
                                                                    <Text style={{ fontSize: 12, padding: 5, color: '#535252' }}>{(!item.pair.milestone.body) ? 'You must now help your PenPal generate a meaningful Milestone. Enter the chat room to learn more.' : `${item.pair.milestone.body}`}</Text>
                                                                    {
                                                                        (item.pair.milestone.completed === false) ? null : <Text>This milestone has been compelted.</Text>
                                                                    }
                                                                    <Text />
                                                                    <View style={{ borderBottomColor: '#515151', borderBottomWidth: .5 }} />
                                                                    <Text style={{ fontSize: 10, padding: 5, color: '#535252' }}> Creator: {`${item.pair.creator.First_Name} ${item.pair.creator.Last_Name}`}</Text>
                                                                    <Text style={{ fontSize: 10, padding: 5, paddingTop: 1, color: '#535252' }}> PenPal: {`${item.creator.First_Name} ${item.creator.Last_Name}`}</Text>
                                                                </View>
                                                            </View>
                                                            : null
                                                        }
                                                    </View>
                                                </TouchableHighlight>
                                                :
                                                <View>
                                                    <View style={styles.snippitc}>
                                                        <Text style={{ fontSize: 12, fontWeight: 'bold', paddingTop: 10, paddingLeft: 4, color: '#535252' }}>{item.title}</Text>
                                                        <Text style={{ fontSize: 12, padding: 5, color: '#535252' }}>{item.body}</Text>
                                                        <Text />
                                                        <Text style={{ fontSize: 12, fontWeight: 'bold', paddingTop: 10, paddingLeft: 4, color: '#535252' }}>Active Milestone</Text>
                                                        <Text style={{ fontSize: 12, padding: 5, color: '#535252' }}>{(!item.milestone) ? 'You must have a PenPal before you can create your next Milestone.' : `${item.milestone.body}`}</Text>
                                                        <Text />
                                                        <View style={{ borderBottomColor: '#515151', borderBottomWidth: .5 }} />
                                                        <Text style={{ fontSize: 10, padding: 5, color: '#535252' }}> Creator: {`${item.creator.First_Name} ${item.creator.Last_Name}`}</Text>
                                                        <Text style={{ fontSize: 10, padding: 5, paddingTop: 1, color: '#535252' }}> PenPal: {(!item.pair) ? 'No PenPal matched yet. Check back later.' : `${item.pair.creator.First_Name} ${item.pair.creator.Last_Name}`}</Text>
                                                    </View>
                                                </View>
                                        )
                                        )}
                                    </View>
                                </ScrollView>
                                : <ChatControl reset={this.toThread} connection={this.state.chat_id} />
                    )
                }}
            </Query>
        )
    };
}

{/* <Archive snippit={item.id} /> */ }

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 15,
        backgroundColor: '#fff',
    },
    container1: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    snippitc: {
        borderWidth: 1,
        borderRadius: 3,
        minHeight: 100,
        margin: 15,
        marginBottom: 4,
        borderColor: '#515151'
    },
    avatar: {
        height: 100,
        borderRadius: 500,
        borderWidth: 60,
        width: 100,
    },
    imageContainer: {
        flex: 1,
        alignItems: 'flex-end'
    },
    refresh: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end'
    }
});
